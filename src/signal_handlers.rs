use notify_rust::{Hint, Notification, NotificationHandle};
use rog_aura::AuraEffect;
use rog_dbus::Signals;
use rog_profiles::Profile;
use supergfxctl::gfx_vendors::GfxVendor;
use slint::Weak;
use std::sync::Arc;
use std::sync::Mutex;
use std::thread::sleep;
use std::thread::spawn;
use std::thread::JoinHandle;
use std::time::Duration;

macro_rules! notify {
    ($notifier:ident, $last_notif:ident, $data:expr) => {
        if let Some(notif) = $last_notif.take() {
            notif.close();
        }
        if let Ok(x) = $notifier($data) {
            $last_notif = Some(x);
        }
    };
}

macro_rules! update_app_item {
    ($item_name:ident, $shared_app:ident, $data:expr) => {
        let app = $shared_app.clone();
        slint::invoke_from_event_loop(move || {
            if let Some(app) = app.upgrade() {
                app.$item_name($data);
            };
        });
    };
}

pub fn start_signal_watch(
    shared_app: Weak<crate::App>,
    signals: Arc<Mutex<Signals>>,
) -> JoinHandle<()> {
    spawn(move || {
        let mut last_notification: Option<NotificationHandle> = None;

        loop {
            if let Ok(lock) = signals.try_lock() {
                if let Ok(data) = lock.led_power_state.try_recv() {
                    update_app_item!(set_leds_checked, shared_app, data.enabled);
                    update_app_item!(set_leds_sleep_checked, shared_app, data.sleep_anim_enabled);
                }

                if let Ok(data) = lock.anime_power_state.try_recv() {
                    update_app_item!(set_anime_checked, shared_app, data.enabled);
                    update_app_item!(set_anime_boot_checked, shared_app, data.boot_anim_enabled);
                };

                if let Ok(data) = lock.bios_sound.try_recv() {
                    update_app_item!(set_sound_checked, shared_app, data);
                }

                if let Ok(data) = lock.bios_gsync.try_recv() {
                    update_app_item!(set_gsync_checked, shared_app, data);
                }

                if let Ok(data) = lock.led_mode.try_recv() {
                    notify!(do_led_notif, last_notification, &data);
                }
                if let Ok(data) = lock.profile.try_recv() {
                    notify!(do_thermal_notif, last_notification, &data);
                }
                if let Ok(data) = lock.charge.try_recv() {
                    notify!(do_charge_notif, last_notification, &data);
                }
                // if let Ok(data) = lock.gfx_vendor.try_recv() {
                //     notify!(do_gfx_notif, last_notification, &data);
                // }
            }
            sleep(Duration::from_millis(100));
        }
    })
}

const NOTIF_HEADER: &str = "ROG Control";

fn do_thermal_notif(profile: &Profile) -> Result<NotificationHandle, notify_rust::error::Error> {
    let icon = match profile {
        Profile::Balanced => "asus_notif_yellow",
        Profile::Performance => "asus_notif_red",
        Profile::Quiet => "asus_notif_green",
    };
    let profile: &str = (*profile).into();
    let x = Notification::new()
        .summary("ASUS ROG")
        .body(&format!(
            "Thermal profile changed to {}",
            profile.to_uppercase(),
        ))
        .hint(Hint::Resident(true))
        .timeout(2000)
        .hint(Hint::Category("device".into()))
        //.hint(Hint::Transient(true))
        .icon(icon)
        .show()?;
    Ok(x)
}

macro_rules! base_notification {
    ($body:expr) => {
        Notification::new()
            .summary(NOTIF_HEADER)
            .body($body)
            .timeout(2000)
            .show()
    };
}

fn do_led_notif(ledmode: &AuraEffect) -> Result<NotificationHandle, notify_rust::error::Error> {
    base_notification!(&format!(
        "Keyboard LED mode changed to {}",
        ledmode.mode_name()
    ))
}

fn do_charge_notif(limit: &u8) -> Result<NotificationHandle, notify_rust::error::Error> {
    base_notification!(&format!("Battery charge limit changed to {}", limit))
}

fn do_gfx_notif(vendor: &GfxVendor) -> Result<NotificationHandle, notify_rust::error::Error> {
    base_notification!(&format!(
        "Graphics mode changed to {}",
        <&str>::from(vendor)
    ))
}
